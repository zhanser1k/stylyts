import Vue from 'vue'
import App from './App.vue'
import router from './router'
import Vue2TouchEvents from 'vue2-touch-events'
import VueFormulate from '@braid/vue-formulate'
import VueTheMask from 'vue-the-mask'
import FullpageModal from 'vue-fullpage-modal'

Vue.use(FullpageModal)
Vue.use(VueTheMask)
Vue.use(VueFormulate)
Vue.use(Vue2TouchEvents)
Vue.config.productionTip = false

new Vue({
  router,
  render: h => h(App)
}).$mount('#app')
