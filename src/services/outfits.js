import { API } from './API';

export const outfitsService = {
  getOutfits: (profileId) => API.get(`/v1/outfits`, {
    params: {
      author: profileId,
      page_size: 9999
    }
  }),
  getOutfitById: (outfitId) => API.get(`/v1/outfits/${outfitId}`)
};
