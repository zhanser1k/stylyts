import { API } from './API';

export const clothesService = {
  getClothes: (params) => API.get('/v1/clothes/', {
    params: {
      ...params,
      page_size: 9999,
    },
  }),
  getClothesById: (id) => API.get(`/v1/clothes/${id}`, { params: { page_size: 9999 } }),
  getClothesTypes: () => API.get('/v1/clothes/types/'),
  getCategories: () => API.get('/v1/clothes/categories/', { params: { page_size: 100 } }),
  getColors: () => API.get('/v1/clothes/colors/', { params: { page_size: 100 } }),
};
