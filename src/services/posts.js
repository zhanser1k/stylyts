import { API } from './API';

export const postsService = {
  getPosts: (profileId) => API.get('/v1/posts', {
    params: {
      author: profileId,
      page_size: 9999
    }
  }),
  getPostById: (id) => API.get(`/v1/posts/${id}`)
};
