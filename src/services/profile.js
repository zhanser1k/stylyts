import { API } from './API';

export const profileService = {
  getProfile: (username) => API.get(`/v1/profiles/retrieve-by-username/${username}`),
};
