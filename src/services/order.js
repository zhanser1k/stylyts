import { API } from './API';

export const orderService = {
  createOrder: (body) => API.post('/v1/orders', body)
};
