import Vue from 'vue'
import VueRouter from 'vue-router'
import Store from '../views/Store';
import ClothesDetail from '../views/ClothesDetail';
import Cart from '../views/Cart';
import Order from '../views/Order';
import OutfitsDetail from '../views/OutfitsDetail';
import PostsDetail from '../views/PostsDetail';
import SuccessOrder from '../views/SuccessOrder';

Vue.use(VueRouter)

const routes = [
  {
    path: '/s/:username',
    component: Store,
    props: true,
  },
  {
    path: '/s/:username/:clothesId',
    component: ClothesDetail,
    props: true,
  },
  {
    path: '/s/:username/outfits/:outfitId',
    component: OutfitsDetail,
    props: true,
  },
  {
    path: '/s/:username/posts/:postId',
    component: PostsDetail,
    props: true,
  },
  {
    path: '/success',
    component: SuccessOrder,
  },
  {
    path: '/cart',
    component: Cart,
  },
  {
    path: '/order',
    component: Order,
  },
];

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

export default router
